//main.js这是项目的核心文件。全局的配置都在这个文件里面配置
import App from './app';
import 'babel-polyfill';
import '../__common/js/eventBus.js';
import Router from 'vue-router';
import router from './config/router.js';
import lazyload from '@mi/milive-ui/directives/m-lazyload';
import './assets/css/login.css';
Vue.use(Router);
Vue.use(lazyload);
Vue.config.debug = ENV_DEV_MODE; //debug模式

/* 路由发生变化修改页面title */
router.beforeEach((to, from, next) => {
    if (to.meta.title) {
        document.title = to.meta.title
    }
    next()
})

new Vue({
    router,
    el: '#root',
    render: h => h(App)
});
