import Router from "vue-router";

export default new Router({
  routes: [
    {
      path: "/",
      name: "actIndex",
      component: (_) => import("../views/actIndex.vue"),
    },
    {
      path: "/warm",
      name: "warm",
      component: (_) => import("../views/warmPage.vue"),
    },
    {
      path: "/pdAnchorNormal",
      name: "pdAnchorNormal",
      component: (_) => import("../views/pdAnchorNormal.vue"),
    },
    {
      path: "/pd",
      name: "pdAnchor",
      component: (_) => import("../views/pdAnchor.vue"),
    },
    {
      path: "/contribution",
      name: "contributionRank",
      component: (_) => import("../views/contributionRank.vue"),
    },
    {
      path: "/pdWarm",
      name: "pdWarm",
      component: (_) => import("../views/pdWarm.vue"),
    },
    // {
    //   path: "/pdPk",
    //   name: "pdPk",
    //   component: (_) => import("../views/pdPk.vue"),
    // },
    {
      path: "/pdPkNormal",
      name: "pdPkNormal",
      component: (_) => import("../views/pdPkNormal.vue"),
    },
    {
      path: "/pdMeet",
      name: "pdMeet",
      component: (_) => import("../views/pdMeet.vue"),
    },
    {
      path: "/pdTask",
      name: "pdTask",
      component: (_) => import("../views/pdTask.vue"),
    },
    {
      path: "/taskPage",
      name: "taskPage",
      component: (_) => import("../views/taskPage.vue"),
    },
  ],
});
