//region 时间
//完整的格式化  var time2 = new Date().format("yyyy-MM-dd hh:mm:ss");
Date.prototype.format = function(fmt) {
  var o = {
    "M+": this.getMonth() + 1, //月份
    "d+": this.getDate(), //日
    "h+": this.getHours(), //小时
    "m+": this.getMinutes(), //分
    "s+": this.getSeconds(), //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    S: this.getMilliseconds(), //毫秒
  };
  //处理年份
  var reYear = /(y+)/;
  var resultYear = reYear.exec(fmt);
  if (resultYear) {
    var yearformatPart = resultYear[0]; //匹配到的格式化字符
    var yearVal = (this.getFullYear() + "").substr(4 - yearformatPart.length);
    fmt = fmt.replace(yearformatPart, yearVal);
  }
  for (var k in o) {
    var re = new RegExp("(" + k + ")");
    var res = re.exec(fmt);
    if (res) {
      var Val = "" + o[k]; //本次需要替换的数据
      var formatPart = res[0]; //匹配到的格式化字符
      var replaceVal =
        formatPart.length == 1 ? Val : ("00" + Val).substr(Val.length);
      fmt = fmt.replace(formatPart, replaceVal);
    }
  }
  return fmt;
};

Date.prototype.addDays = function(days) {
  let newo = new Date(this.valueOf());
  var olddays = newo.getDate();
  var newdays = olddays + days;
  newo.setDate(newdays);
  return newo;
};

/**
 * 获取当前N分钟内的结束时间。如
 * 当前时间为2021-06-04 19:38:00,则计算出结束时间2021-06-04 19:40:00
 * @param unit 默认为10。若传5，则为获取5分钟内的结束时间，如当前时间为2021-06-04 19:32:00,则计算出结束时间2021-06-04 19:35:00；当前时间为2021-06-04 19:38:00,则计算出结束时间2021-06-04 19:40:00
 * @returns {Date}
 */
Date.prototype.getMinutesEndDate = function(unit) {
  if (!unit) unit = 10;
  let newo = new Date(this.valueOf());
  let curM = newo.getMinutes();
  let target = parseInt((curM + unit) / unit) * unit;
  newo.setMinutes(target);
  newo.setSeconds(0);
  return newo;
};

export const getCountDownMinute = (seconds) => {
  let minute = Math.floor(seconds / 60);
  let sec = Math.floor(seconds % 60);
  if (sec >= 0 && sec <= 9) {
    sec = "0" + sec;
  }
  return minute + ":" + sec;
};

export const getCountDownHms = (seconds) => {
  let day = Math.floor(seconds / 60 / 60 / 24);
  let hour = Math.floor((seconds / 3600) % 24);
  hour = hour + day * 24;
  let minute = Math.floor((seconds / 60) % 60);
  let sec = Math.floor(seconds % 60);
  if (minute >= 0 && minute <= 9) {
    minute = "0" + minute;
  }
  if (sec >= 0 && sec <= 9) {
    sec = "0" + sec;
  }
  return hour + ":" + minute + ":" + sec;
};

export const getCountDownHms1 = (seconds, type) => {
  let day = Math.floor(seconds / 60 / 60 / 24);
  let hour = Math.floor((seconds / 3600) % 24);
  if (type) hour = hour + day * 24;
  if (hour >= 0 && hour <= 9) {
    hour = "0" + hour;
  }
  let minute = Math.floor((seconds / 60) % 60);
  let sec = Math.floor(seconds % 60);
  if (minute >= 0 && minute <= 9) {
    minute = "0" + minute;
  }
  if (sec >= 0 && sec <= 9) {
    sec = "0" + sec;
  }

  return {
    d: day,
    h: hour,
    m: minute,
    s: sec,
  };
};

export const getDayDta = (ds, de) => {
  let s = ds.getTime();
  let e = de.getTime();
  let ret = (e - s) / (1000 * 60 * 60 * 24);
  return parseInt(ret);
};

export const getTodayStartDayDta = () => {
  let maxDaysIndex = getDayDta(start, end);
  let ret = getDayDta(start, today);
  if (ret > maxDaysIndex) ret = maxDaysIndex;
  return ret;
};

export const getCurDay = () => {
  let todayTime = today.getTime();
  let endDayTime = end.getTime();
  if (todayTime - endDayTime > 0) {
    return end.format("yyyyMMdd");
  }
  return today.format("yyyyMMdd");
};
//endregion

//region 赛段
export const startStage = () => {
  let curDate = new Date();
  curDate.setHours(startHour);
  curDate.setMinutes(0);
  curDate.setSeconds(0);
  return curDate;
};
export const stageMinutes = 10;
export const stageTitle = ["64进32", "32进16", "16进8", "8进4", "4进2", "决赛"];
export const getEndStage = (level) => {
  // console.log('getEndStage startStage='+startStage())

  let toadd = level * stageMinutes * 60 * 1000;
  // console.log('getEndStage toadd='+toadd)
  let target = startStage().getTime() + toadd;
  // console.log('getEndStage target='+target)
  return new Date(target).format("hh:mm");
};
//endregion

//todo 线上环境
export const today = new Date()
export const start = new Date('2021/08/04 00:00:00')
export const end = new Date('2021/08/09 23:59:59')
export const startHour = 17
export const endHour = 18

//测试情况
// export const today = new Date()
// export const start = new Date('2021/05/27 00:00:00')
// export const end = new Date('2021/05/29 23:59:59')
// export const startHour = 18
// export const endHour = 19

//region 文本显示
export const startDate = parseInt(start.format("dd"));
export const endDate = parseInt(end.format("dd"));
export const bannerTxt = start.format("M月dd日") + "-" + end.format("M月dd日");
export const pdWarmTxt = start.format("MM月dd日") + startHour + "点";
export const pdPkTxt = `${startHour}:00-${endHour}:00`;
//endregion
