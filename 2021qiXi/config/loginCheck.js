let util = require("./mi.util.js");
let $ = require("../../__common/js/zepto.js");

var _location = window.location.href,
    zuid = util.getQueryVal('zuid', _location),
    pf = util.getQueryVal('pf', _location),
    activity = util.getQueryVal('actId', _location),
    origin = util.getQueryVal('origin', _location);


// 微信小程序的appid：wx716c2407eee83bab




/**
 * @desc 参数实例化构造函数，使用时实例化
 */
function Config() {
    if (!(this instanceof Config)) {
        throw new Error('Cannot invoke this Class without keyword "new".');
    }

    var self = this;
    //服务端获取的页面参数
    self.username = '';
    self.userId = 1231123;
    // self.status = status;
    self.lid = '123131_1231313';
    self.zuid = 12313123;
    self.avator = 1233123123;
    self.v_id = 1231312;
    self.v_nickname = 'ahadag哈哈',
    //判断当前运行环境
    self.cur_host = window.location.host;
    self.isStaging = self.cur_host == "staging.weixin.zb.mi.com";

    //获取用户uid & token
    self.uuid = util.getCookie('zhiboUuid');
    self.serviceToken = util.getCookie('zhiboServiceToken');
    self.commonTips = {
        distance: '30%',
        duration: 500,
        stopTime: 1000,
        style: 'linear',
        callback: function () {}
    };
    //设置cookie
    self.cookieKey = self.uuid;
    self.cookieUid = util.getCookie(self.cookieKey);
    if (!self.cookieUid) {
        util.setCookie(self.cookieKey, self.serviceToken);
        self.cookieUid = util.getCookie(self.cookieKey);
    }
    //分享的一些参数
    self.shareConfig = {
        desc: '我在看' + self.decodeString(self.username) + '(直播号: ' + self.userId + ')在' + self.position + '直播' + self.decodeString(self.video_title) + '，',
        title: '“' + self.decodeString(self.username) + '”在直播'
    };
    self.url = window.location.href;
    self.devWidth = screen.width > screen.height ? screen.height : screen.width;
    self.devHeight = screen.width > screen.height ? $(window).width() : $(window).height();
    //初始化语言
    self.lan = util.language.which();
    //系统语言是否是中文
    self.isChinese = self.lan == 'zh_cn' || self.lan == 'zh_tw';
    //如果系统语言不是中文则统一使用英文环境
    if (!self.isChinese) {
        self.lan = 'en';
    }

    //获取链接中的活动名称，分享人id
    /*self.activity = activity;
     self.origin = origin;*/
}


/**
 * @desc 格式化unicode字符串
 * @param {String} unicodeStr
 * @returns {String}
 */
Config.prototype.unicodeToString = function (unicodeStr) {
    var self = this;
    return unicodeStr.replace(/\\u[\dA-Fa-f]{4}/g, function (match) {
        return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
    });
};

/**
 * @desc 格式化HTML entity code字符串, 并进行unicode转码
 * @param {String} encodeStr
 * @returns {String}
 */
Config.prototype.decodeString = function (encodeStr) {
    var self = this;
    var $element = $(document.createElement('div'));
    $element.html(encodeStr);
    return self.unicodeToString($element.text());
};

/**
 * @desc 根据pf的类型返回share参数值
 * @param {String} pf pf参数
 */
Config.prototype.whatShare = function (pf) {
    switch (pf) {
        case "qq":
            return 2;
        case "wechat":
            return 1;
        case "moment":
            return 1;
        case "weibo":
            return 3;
        case "fb":
            return 4;
        case "instagram":
            return 6;
        case "twitter":
            return 5;
        case "whatsapp":
            return 7;
    }
};

/**
 * @desc 没有回放的html组件
 */
Config.prototype.getDOM = function (p_msg, a_msg, index) {
    return '<div class="nodata-warp">' +
        '<p class="nodata">' + p_msg + '</p>' +
        '</div>';
};

/**
 * @desc 截取长字符
 * @param {String} str 表示需要截取的字符串
 */
Config.prototype.changeLongTxt = function (str) {
    if (typeof str == "object") {
        str = JSON.stringify(str);
    }
    var len = 0;
    if (str.length) {
        len = str.replace(/[^\x00-\xff]/g, "01").length;
    } else {
        return "***";
    }
    if (len > 6) { //如果太长显示省略号  汉子是超过三个个字符，字母超过6个字符
        if (str.length != len) {
            return str.substring(0, 3) + "…";
        } else {
            return str.substring(0, 6) + "…";
        }
    }
    if (!str) {
        return "***";
    }
    return str;
};

/**
 * @desc 解码
 */
Config.prototype.uncode = function (str) {
    return str.replace(/&#(x)?([^&]{1,5});?/g, function (a, b, c) {
        return String.fromCharCode(parseInt(c, b ? 16 : 10));
    });
};

/**
 * @desc 重新渲染tab高度
 */
Config.prototype.resize = function (index) {
    $('#slider').css({
        height: $('.content-item').eq(index).height() + 'px'
    });
};

Config.prototype.resizeImg = function (tag) {
    var _img = $('.content-item img.avator');
    setTimeout(function () {
        _img.css({
            height: _img.width() + 'px'
        });
        if (!tag) {
            $('#slider').css({
                height: $('.content-item').eq($('.nav-warp li.active').index()).height() + 'px'
            });
        }
    }, 450);
};

/**
 * @desc 主要用于微信的页面刷新
 */
Config.prototype.updateUrl = function (url, key) {
    var key = (key || 't') + '='; //默认是"t"
    var reg = new RegExp(key + '\\d+'); //正则：t=1472286066028
    var timestamp = +new Date();
    if (url.indexOf(key) > -1) { //有时间戳，直接更新
        return url.replace(reg, key + timestamp);
    } else { //没有时间戳，加上时间戳
        if (url.indexOf('\?') > -1) {
            var urlArr = url.split('\?');
            if (urlArr[1]) {
                return urlArr[0] + '?' + key + timestamp + '&' + urlArr[1];
            } else {
                return urlArr[0] + '?' + key + timestamp;
            }
        } else {
            if (url.indexOf('#') > -1) {
                return url.split('#')[0] + '?' + key + timestamp + location.hash;
            } else {
                return url + '?' + key + timestamp;
            }
        }
    }
};

// /**
//  * @desc 用户实例化构造函数，使用时实例化
//  */
// function User() {
//     if (!(this instanceof User)) {
//         throw new Error('Cannot invoke this Class without keyword "new".');
//     }

//     var self = this;
//     self.follow_click = true;
//     self.join_click = true;
//     //防止按钮多次点击
//     self.isClick = true;
//     self.push_click = true;
//     //push notification
//     self.push1 = 0;
//     self.push2 = 0;
//     self.push3 = 0;
// }

// /**
//  * @desc 关注主播
//  */
// User.prototype.follow = function () {
//     var self = this;
//     var $target = $('.follow');
//     if (!self.follow_click) {
//         return;
//     }
//     self.follow_click = false;

//     $.ajax({
//         url: config.isStaging ? 'http://10.98.80.85:8090/zhibo/relation_cgi' : 'http://bi.zb.mi.com/zhibo/relation_cgi',
//         data: {
//             m: "follow",
//             uuid: config.uuid,
//             st: config.serviceToken,
//             followUuid: config.userId,
//             jsonp: "follow"
//         },
//         dataType: 'jsonp',
//         jsonpCallback: 'follow',
//         success: function (res) {
//             if (res.code == 0) {
//                 if (config.lan == 'en') {
//                     $target.unbind('touchend').removeClass('follow').addClass('unfollow').text("Following");
//                 } else {
//                     $target.unbind('touchend').removeClass('follow').addClass('unfollow').text("已关注");
//                 }

//                 util.sendStatistic({
//                     curpageid: 'zhiboshare_follow',
//                     type: 'zhiboshare_follow',
//                     uid: util.getCookie('zhiboUuid'),
//                     fuid: 'follow',
//                     trace: config.lan,
//                     cid: 'zhiboshare_activity',
//                     from: config.pf,
//                     position: 'success_' + $target.attr('uuid')
//                 }, function () {});

//                 var _msginfo = "关注成功";
//                 if (config.lan == 'en') {
//                     _msginfo = "Followed successfully"
//                 }
//                 Tips.t($.extend(config.commonTips, {
//                     msg: _msginfo,
//                     callback: function () {
//                         self.follow_click = true;
//                         window.location.href = 'walilive://update/follow?uuid=' + $target.attr('uuid');
//                     }
//                 }));
//             }
//             if (util.app.isWeixin()) {
//                 self.goPush(3);
//             }
//         },
//         error: function (err) {
//             util.sendStatistic({
//                 curpageid: 'zhiboshare_follow',
//                 type: 'zhiboshare_follow',
//                 uid: util.getCookie('zhiboUuid'),
//                 fuid: 'follow',
//                 cid: 'zhiboshare_activity',
//                 from: config.pf,
//                 trace: config.lan,
//                 position: 'failed_' + $target.attr('uuid')
//             }, function () {});

//             var _msginfo = "关注失败";
//             if (config.lan == 'en') {
//                 _msginfo = "Oops! Please try again"
//             }
//             Tips.t($.extend(config.commonTips, {
//                 msg: _msginfo,
//                 callback: function () {
//                     self.follow_click = true;
//                 }
//             }));
//             if (util.app.isWeixin()) {
//                 self.goPush(3);
//             }
//         }
//     });
// };

// /**
//  * @desc 加入房间
//  * @param {Number} share 分辨分享的来源
//  */
// User.prototype.joinRoom = function (share) {
//     var self = this;
//     if (!self.join_click) {
//         return;
//     }
//     self.join_click = false;

//     $.ajax({
//         url: config.isStaging ? 'http://10.98.80.87:8686/join' : 'http://pmsg.zb.mi.com/join',
//         data: {
//             from: config.uuid,
//             room: config.lid,
//             ts: Date.now(),
//             anchor: config.zuid,
//             share: share,
//             uuid: config.uuid,
//             st: config.serviceToken
//         },
//         dataType: 'jsonp',
//         jsonpCallback: 'join',
//         success: function (res) {
//             self.join_click = true;
//         },
//         error: function (err) {
//             self.join_click = true;
//         }
//     });
// };

// /**
//  * @desc 唤起app
//  * @param {Boolean} init init参数
//  */
// User.prototype.openApp = function (init) {
//     var self = this;
//     if (self.isClick) {
//         self.isClick = false;
//         util.sendStatistic({
//             curpageid: 'zhiboshare_openApp',
//             trace: config.lan,
//             type: 'zhiboshare_openApp',
//             uid: config.cookieUid,
//             fuid: 'open_app',
//             cid: 'zhiboshare_activity',
//             from: config.pf
//         }, function () {});

//         if (util.sys.isIOS()) {
//             window.download.openApp({
//                 islive: config.status,
//                 flv: config.flv,
//                 m3u8: config.m3u8,
//                 playback: config.playback,
//                 lid: config.lid,
//                 zuid: config.zuid,
//                 videoUrl: config.videoUrl,
//                 type: config.type,
//                 useUniveralLink: util.sys.isIOS(),
//                 init: init
//             }, function () {
//                 self.isClick = true;
//             });
//         } else {
//             self.isClick = true;
//             var schema = 'walilive://';
//             if (config.islive == 1) {
//                 schema += 'room/join?liveid=' + config.lid + '&playerid=' + config.zuid + '&videourl=' + encodeURIComponent(config.videoUrl);
//             } else if (config.islive == 2) {
//                 schema += 'playback/join?liveid=' + config.lid + '&playerid=' + config.zuid + '&videourl=' + encodeURIComponent(config.videoUrl);
//             } else if (config.islive == 0) {
//                 schema += 'room/join?liveid=' + config.lid + '&playerid=' + config.zuid;
//             }

//             if (config.type == 8) { //打开电台加上type
//                 schema += '&type=10';
//             }

//             window.download.androidOpenApp(schema, function (t) {
//                 if (t) {
//                     // window.location.href = 'https://s1.zb.mi.com/universal/guide.html';
//                     window.location.href = 'https://live.wali.com/lang/cn/index.html';
//                 }
//             });
//         }
//     }
// };

// /**
//  * @desc 安卓下发调起app的push notification
//  * @param {Number} type push到服务端的参数
//  */
// User.prototype.goPush = function (type) {
//     var self = this;
//     if (util.sys.isAndroid()) {
//         if (!self.push_click) {
//             return;
//         }
//         if (type == 1 && self.push1 >= 3) {
//             return;
//         } else if (type == 2 && self.push2 >= 3) {
//             return;
//         } else if (type == 3 && self.push3 >= 3) {
//             return;
//         }
//         self.push_click = false;
//         $.ajax({
//             url: config.isStaging ? 'http://10.98.80.87:8686/push' : 'http://pmsg.zb.mi.com/push',
//             data: {
//                 from: config.uuid,
//                 room: config.lid,
//                 ts: Date.now(),
//                 type: config.type,
//                 anchor: config.zuid,
//                 uuid: config.uuid,
//                 st: config.serviceToken
//             },
//             dataType: 'jsonp',
//             jsonpCallback: 'push',
//             success: function (res) {
//                 if (res.code == 0) {
//                     if (type == 1) {
//                         self.push1++;
//                     } else if (type == 2) {
//                         self.push2++;
//                     } else if (type == 3) {
//                         self.push3++;
//                     }
//                 }
//                 self.push_click = true;
//             },
//             error: function (err) {
//                 self.push_click = true;
//             }
//         });
//     }
// };

// /**
//  * @desc 下载app
//  * @param {Object} conf 下载打点的参数
//  */
// User.prototype.download = function (conf) {
//     util.sendStatistic(conf, function () {});
//     window.download.downloadFromVideo();
// };


let config = new Config();

/**
 * @desc login构造函数，使用时实例化
 */
function Login() {
    if (!(this instanceof Login)) {
        throw new Error('Cannot invoke this Class without keyword "new".');
    }

    var self = this;

    // 用户是否登陆了？
    self.isLegalLogin = !!config.uuid && !!config.serviceToken;
    //重定向的域名
    self.stagingRedirect = {
        mi: 'http://staging.weixin.zb.mi.com/oauth/login?type=4&share_url=',
        wx: 'http://staging.weixin.zb.mi.com/oauth/login?share_url='
    };
    self.onlineRedirect = {
        mi: 'http://weixin.zb.mi.com/oauth/login?type=4&share_url=',
        wx: 'http://weixin.zb.mi.com/oauth/login?share_url='
    };
    //后缀的参数
    self.param = {
        mi: '&response_type=code',
        wx: '&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect'
    };
    self.appid = config.isStaging ? 'wx82881452522d0cc7' : 'wx0f7c40ebbcf236fe';

    //登录账户的域名，注意测试环境和线上环境appid
    self.accountHost = {
        mi: 'https://account.xiaomi.com/oauth2/authorize?client_id=2882303761517438806&redirect_uri=',
        wx: 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' + self.appid + '&redirect_uri='
    };
    //登录弹窗
    self.login_alert = [
        {
            tag: '<div></div>',
            className: 'login_alert',
            parentNodeSelector: '#bg_mask'
        },
        {
            tag: '<div></div>',
            className: 'login_close_con',
            parentNodeSelector: '.login_alert'
        },
        {
            tag: '<div></div>',
            className: 'login_close',
            parentNodeSelector: '.login_close_con'
        },
        {
            tag: '<p>请使用常用账号</p>',
            className: 'login_title',
            parentNodeSelector: '.login_alert'
        }
    ];
    self.login_common_event = [
        {
            selector: '.login_close_con',
            eventType: 'touchend',
            listener: function (event) {
                $('#bg_mask').remove();
                event.preventDefault();
            }
        },
        {
            selector: '.login_btn_wx',
            eventType: 'touchend',
            listener: function (event) {
                $('#bg_mask').remove();
                self.userLogin('wx');
                event.preventDefault();
            }
        },
        {
            selector: '.login_btn_mi',
            eventType: 'touchend',
            listener: function (event) {
                $('#bg_mask').remove();
                self.userLogin('mi');
                event.preventDefault();
            }
        }
    ];
    //用户信息弹窗
    self.login_alert_info = [{
            tag: '<div></div>',
            className: 'login_alert_info',
            parentNodeSelector: '#bg_mask'
        },
        {
            tag: '<div></div>',
            className: 'login_close_con',
            parentNodeSelector: '.login_alert_info'
        },
        {
            tag: '<div></div>',
            className: 'login_close',
            parentNodeSelector: '.login_close_con'
        },
        {
            tag: '<div><img src="' + config.avator + '" /></div>',
            className: 'avator lazy user_info',
            parentNodeSelector: '.login_alert_info'
        },
        {
            tag: '<p>' + config.unicodeToString(config.v_nickname) + '</p>',
            className: 'user_name',
            parentNodeSelector: '.login_alert_info'
        },
        {
            tag: '<p>直播号码</p>',
            className: 'user_number',
            parentNodeSelector: '.login_alert_info'
        },
        {
            tag: '<div><p>退出</p></div>',
            className: 'quit_login',
            parentNodeSelector: '.login_alert_info'
        }
    ];
    self.login_common_event_info = [
        {
            selector: '.login_close_con',
            eventType: 'touchend',
            listener: function (event) {
                $('#bg_mask').remove();
                event.preventDefault();
            }
        },
        {
            selector: '.login_close',
            eventType: 'touchend',
            listener: function (event) {
                $('#bg_mask').remove();
                event.preventDefault();
            }
        },
        {
            selector: '.quit_login',
            eventType: 'touchend',
            listener: function (event) {
                $('#bg_mask').remove();
                event.preventDefault();
                login.quitConfirmMask();
            }
        }
    ];
    //退出确认弹窗
    self.login_alert_confirm = [
        {
            tag: '<div></div>',
            className: 'login_alert_confirm',
            parentNodeSelector: '#bg_mask'
        },
        {
            tag: '<div><p>是否确认退出？</p></div>',
            className: 'confirm_quit',
            parentNodeSelector: '.login_alert_confirm'
        },
        {
            tag: '<div></div>',
            className: 'quit_btn_con clear',
            parentNodeSelector: '.login_alert_confirm'
        },
        {
            tag: '<div>取消</div>',
            className: 'quit_cancel',
            parentNodeSelector: '.quit_btn_con'
        },
        {
            tag: '<div>确认</div>',
            className: 'quit_confirm_btn',
            parentNodeSelector: '.quit_btn_con'
        }
    ];
    self.login_common_event_confirm = [
        {
            selector: '.quit_cancel',
            eventType: 'touchend',
            listener: function (event) {
                $('#bg_mask').remove();
                event.preventDefault();
            }
        },
        {
            selector: '.quit_confirm_btn',
            eventType: 'touchend',
            listener: function (event) {
                $('#bg_mask').remove();
                event.preventDefault();
                //清除uid & token 的cookie
                document.cookie = "zhiboUuid=;path=/;domain=.mi.com;max-age=0;expires=" + new Date(0).toUTCString();
                document.cookie = "zhiboServiceToken=;path=/;domain=.mi.com;max-age=0;expires=" + new Date(0).toUTCString();
                if (util.app.isWeixin() && util.sys.isAndroid()) {
                    window.location.href = config.updateUrl(window.location.href);
                } else {
                    location.reload(true);
                }
            }
        }
    ];
    self.pingTimer = null;
    self.notJoin = 0;
}

/**
 * @desc 登录
 * @param {String} type mi: 小米登录，wx: 微信登陆
 */
Login.prototype.userLogin = function (type) {
    var self = this;
    var redirect_url = self.onlineRedirect[type] + encodeURIComponent(window.location.href);
    var url = self.accountHost[type] + encodeURIComponent(redirect_url) + self.param[type];

    //测试环境则使用测试的重定向
    if (config.isStaging) {
        redirect_url = self.stagingRedirect[type] + encodeURIComponent(window.location.href);
        url = self.accountHost[type] + encodeURIComponent(redirect_url) + self.param[type];
    }

    console.log('最终重定向的', url)

    // url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx0f7c40ebbcf236fe&redirect_uri=http%3A%2F%2Fweixin.zb.mi.com%2Foauth%2Flogin%3Fshare_url%3Dhttp%253A%252F%252F10.235.234.47%253A9001%252Fdev%252FAprilFool%252F%2523%252F&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect"

    // url = "http://weixin.zb.mi.com/oauth/login?share_url=http%3A%2F%2F10.235.234.47%3A9001%2Fdev%2FAprilFool%2F%23%2F"
    // alert(url)
    window.location.href = url;
};

/**
 * @desc 登录弹出框
 */
Login.prototype.loginMask = function () {
    var self = this;
    //微信显示微信登陆和小米登陆, 非微信显示小米登录
    if (util.app.isWeixin()) {
        var wx_alert = [{
                tag: '<div class="login_btn_is_wx"><span class="icon_login"><i class="icon_wx"></i></span><p class="p_wx">微信账号登录</p></div>',
                className: 'login_btn_wx login_btn',
                parentNodeSelector: '.login_alert'
            },
            {
                tag: '<div></div>',
                className: 'login_other',
                parentNodeSelector: '.login_alert'
            },
            {
                tag: '<div>小米账号登录</div>',
                className: 'login_btn_mi login_btn_bottom',
                parentNodeSelector: '.login_other'
            }
        ];
        util.mask('mask_bg', self.login_alert.concat(wx_alert), self.login_common_event);
    } else {

        var other_alert = [
            {
                tag: '<div><span class="icon_login"><i class="icon_mi"></i></span><p class="p_mi">小米账号登录</p></div>',
                className: 'login_btn_mi login_btn',
                parentNodeSelector: '.login_alert'
            }
        ];

        util.mask('mask_bg', self.login_alert.concat(other_alert), self.login_common_event);
    }

    $('.mask_bg').css('width', '100%');
};

/**
 * @desc 用户信息弹出框
 */
Login.prototype.loginInfoMask = function () {
    var self = this;
    util.mask('mask_bg', other_alert, self.login_common_event_info);
    $('.mask_bg').css('width', config.devWidth + 'px');
};

/**
 * @desc 退出确认弹出框
 */
Login.prototype.quitConfirmMask = function () {
    var self = this;
    util.mask('mask_bg', self.login_alert_confirm, self.login_common_event_confirm);
    $('.mask_bg').css('width', config.devWidth + 'px');
};

/**
 *@desc 登录状态轮询告知后台
 */
Login.prototype.isLoginPing = function () {
    var self = this;
    if (login.isLegalLogin && util.app.isWeixin() && self.pingTimer == null) {
        self.pingTimer = setInterval(function () {
            $.ajax({
                url: config.isStaging ? 'http://10.98.80.87:8686/ping' : 'http://pmsg.zb.mi.com/ping',
                data: {
                    from: config.uuid,
                    room: config.lid,
                    anchor: config.zuid,
                    uuid: config.uuid,
                    st: config.serviceToken
                },
                dataType: 'jsonp',
                jsonpCallback: 'ping',
                success: function (res) {

                    if (self.notJoin == 0) {
                        var share = config.whatShare(config.pf);
                        $.ajax({
                            url: config.isStaging ? 'http://10.98.80.87:8686/join' : 'http://pmsg.zb.mi.com/join',
                            data: {
                                from: config.uuid,
                                room: config.lid,
                                ts: Date.now(),
                                anchor: config.zuid,
                                share: share,
                                uuid: config.uuid,
                                st: config.serviceToken
                            },
                            dataType: 'jsonp',
                            jsonpCallback: 'join',
                            success: function (res) {
                                console.log('end ping join')
                            },
                            error: function (err) {}
                        });
                        self.notJoin = 1;
                    }
                },
                error: function (err) {
                    console.log(err)
                }
            });
        }, 5000);
    }
};


export default Login;