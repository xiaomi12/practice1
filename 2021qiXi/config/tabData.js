import { start } from "./timeConfig";
export const tabData = [
  {
    id: 1,
    stage: 0,
    name: "相遇",
    mainTitle: "N进60",
    time: start.format("M.d"), //'7.26',
    date: start.format("yyyyMMdd"), //'202107026'
  },
  {
    id: 2,
    stage: 1,
    name: "相知",
    mainTitle: "60进40",
    time: start.addDays(1).format("M.d"), //'7.27',
    date: start.addDays(1).format("yyyyMMdd"), //'20210727'
  },
  {
    id: 3,
    stage: 2,
    name: "相恋",
    mainTitle: "40进24",
    time: start.addDays(2).format("M.d"), //'7.28',
    date: start.addDays(2).format("yyyyMMdd"), //'20210728'
  },
  {
    id: 4,
    name: "相伴",
    mainTitle: "24进12",
    time: `${start.addDays(3).format("M.d")}-${start.addDays(5).format("M.d")}`, //'7.29-7.31',
    secondaryTab: [
      {
        id: 6,
        stage: 3,
        name: "相伴",
        mainTitle: "24进20",
        time: start.addDays(3).format("M.d"), //'7.29',
        date: start.addDays(3).format("yyyyMMdd"), //'20210729'
      },
      {
        id: 7,
        stage: 4,
        name: "相伴",
        mainTitle: "20进16",
        time: start.addDays(4).format("M.d"), //'7.30',
        date: start.addDays(4).format("yyyyMMdd"), //'20210730'
      },
      {
        id: 8,
        stage: 5,
        name: "相伴",
        mainTitle: "16进12",
        time: start.addDays(5).format("M.d"), //'7.31',
        date: start.addDays(5).format("yyyyMMdd"), //'20210731'
      },
    ],
  },
  {
    id: 9,
    stage: 6,
    name: "相守",
    mainTitle: "12决5",
    time: start.addDays(6).format("M.d"), //'8.1',
    date: start.addDays(6).format("yyyyMMdd"), //'20210801',

    // id: 5,
    // name: "相守",
    // mainTitle: "12决5",
    // time: start.addDays(6).format("M.d"), //'8.1',
    // secondaryTab: [
    //   {
    //     id: 9,
    //     stage: 6,
    //     name: "相守",
    //     mainTitle: "相守榜单",
    //     time: start.addDays(6).format("M.d"), //'8.1',
    //     date: start.addDays(6).format("yyyyMMdd"), //'20210801',
    //   },
    //   {
    //     id: 10,
    //     mainTitle: "挂件PK榜",
    //     mainIndex: 4,
    //     secondaryIndex: 1,
    //     isPK: true,
    //   },
    // ],
  },
];

export function changeTabDateWithStartTime(startTime) {
  let startDate = new Date(startTime);
  tabData.forEach((item) => {
    if (item.secondaryTab) {
      if (item.id === 4) {
        item.time = `${startDate.addDays(3).format("M.d")}-${startDate
          .addDays(5)
          .format("M.d")}`;
      }
      if (item.id === 5) {
        item.time = startDate.addDays(6).format("M.d");
      }
      item.secondaryTab = item.secondaryTab.map((secondaryTab) => {
        if (!secondaryTab.isPK) {
          const newDate = startDate.addDays(secondaryTab.stage);
          secondaryTab.time = newDate.format("M.d");
          secondaryTab.date = newDate.format("yyyyMMdd");
        }
        return secondaryTab;
      });
    } else {
      const newDate = startDate.addDays(item.stage);
      item.time = newDate.format("M.d");
      item.date = newDate.format("yyyyMMdd");
    }
  });
  return tabData;
}
/**
 *
 * @param {*} stage  当前赛段
 * @param {*} showPk 是否显示 PK 赛段
 * @returns
 */
export function getTodayTab(stage = -1) {
  let findTab = {};
  if (stage < 0) stage = 0;
  if (stage > 6) stage = 6;
  tabData.forEach((item, mainIndex) => {
    if (item.secondaryTab) {
      item.secondaryTab.forEach((secondaryTab, secondaryIndex) => {
        if (secondaryTab.stage === stage) {
          findTab = Object.assign({}, secondaryTab, {
            mainIndex,
            secondaryIndex,
          });
        }
      });
    } else {
      if (item.stage === stage) {
        findTab = Object.assign({}, item, {
          mainIndex,
          secondaryIndex: 0,
        });
      }
    }
  });
  return findTab;
}

export const getPkTab = () => {
  return tabData[tabData.length - 1].secondaryTab[1];
};
