module.exports = {
  dev: {
    CUR_ENV: "dev",
    ACT_ID: 201, // 201 暂时先用 275 有数据
    GET_INFO: "//10.38.164.87:10825/activityAutoPlatform/rank", // 获取主播信息
    GET_PK: "//10.38.164.87:10825/activityAutoPlatform/pk", //PK
    GET_SIGN: "//10.38.164.87:10825/activityAutoPlatform/task", //签到
    LOTTERY: "//10.38.164.87:10825/activityAutoPlatform/lottery",
    GET_CUR_TIME: "//act.zb.mi.com/activityAutoPlatform/rank/t", // 获取当前时间戳

    // ACT_ID: 279,
    // GET_INFO: "//act.zb.mi.com/activityAutoPlatform/rank", // 获取主播冲关信息
    // GET_PK: "//act.zb.mi.com/activityAutoPlatform/pk", //PK
    // GET_SIGN: "//act.zb.mi.com/activityAutoPlatform/task", //签到
    // LOTTERY: "//act.zb.mi.com/activityAutoPlatform/lottery",
    // GET_CUR_TIME: "//act.zb.mi.com/activityAutoPlatform/rank/t", // 获取当前时间戳
  },
  prod: {
    CUR_ENV: "prod",
    ACT_ID: 279,
    GET_INFO: "//act.zb.mi.com/activityAutoPlatform/rank", // 获取主播冲关信息
    GET_PK: "//act.zb.mi.com/activityAutoPlatform/pk", //PK
    GET_SIGN: "//act.zb.mi.com/activityAutoPlatform/task", //签到
    LOTTERY: "//act.zb.mi.com/activityAutoPlatform/lottery",
    GET_CUR_TIME: "//act.zb.mi.com/activityAutoPlatform/rank/t", // 获取当前时间戳
  },
};
