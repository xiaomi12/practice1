import axios from "axios";
import { testRankList } from "./config/testData";
import coreBus from "@mi/milive-ui/utils/event-bus";
import { MiUtil } from "@mi/milive-util";
import {getCountDownMinute} from "./config/timeConfig";
let util = new MiUtil();

let _location = window.location.href,
  uuid = util.getCookie("zhiboUuid"),
  st = util.getCookie("zhiboServiceToken"),
  zuid = util.getQueryVal("zuid", _location),
  tab = util.getQueryVal("tab", _location),
  showPk = util.getQueryVal("showPk", _location),
  env = util.getQueryVal("env", _location),
  ctype = util.sys.isAndroid() ? 2 : 1,
  pkGroupName = "battle",
  rankName = "total";

// 添加响应拦截器
axios.interceptors.response.use(
  (response) => {
    const errorMap = new Map([
      [-2, "测试账号"],
      [-1, "error"],
      [1, "参数错误"],
      [5, "非比赛时段"],
      [4, "不是参赛主播"],
    ]);
    if (errorMap.has(response.data.code)) {
      coreBus.$emit("MiLive-UI:tips", {
        msg: errorMap.get(response.data.code),
      });
    }

    return response;
  },
  (error) => {
    const errorMap = new Map([
      [400, "错误请求"],
      [401, "验证失败"],
      [403, "拒绝访问"],
      [404, "活动未开始~"],
      [405, "请求方法不允许"],
      [500, "服务器错误"],
    ]);

    if (error && error.response) {
      if (errorMap.has(error.response.status)) {
        coreBus.$emit("MiLive-UI:tips", {
          msg: errorMap.get(error.response.status),
        });
      }
    }
  }
);

/**
 * 检查是否登录
 */
export let checkIsLogin = () => {
  var ua = navigator.userAgent;
  var lowerUa = ua.toLowerCase();
  if (!(uuid && st)) {
    if (util.app.isZhiBo()) {
      window.location.href = "walilive://unlogin?closeWebview=true";
    } else if (/micromessenger/.test(lowerUa)) {
      const REDIRECT_URI =
        "http://weixin.zb.mi.com/oauth/login?share_url=" +
        encodeURIComponent(window.location.href);

      window.location.href =
        "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx0f7c40ebbcf236fe&redirect_uri=" +
        encodeURIComponent(REDIRECT_URI) +
        "&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect";
    } else {
      download();
    }
    return false;
  } else {
    return true;
  }
};

export const download = (_) => {
  var ua = navigator.userAgent;
  var lowerUa = ua.toLowerCase();
  if (!/micromessenger/.test(lowerUa)) {
    let url = encodeURIComponent(`${window.location.href.split("?")[0]}`);
    // location=`walilive://openurl/newwindow?url=${url}&sharable=true&ishalf=false`;
    location = `walilive://channel?page=0&channel_id=59`;
    setTimeout(function() {
      location = checkPlatform();
    }, 5 * 1000);
  } else {
    location = checkPlatform();
  }
};

// 检查平台
function checkPlatform() {
  var ua = navigator.userAgent;
  var lowerUa = ua.toLowerCase();
  if (/micromessenger/.test(lowerUa)) {
    return "http://a.app.qq.com/o/simple.jsp?pkgname=com.wali.live";
  } else if (/iphone/.test(lowerUa)) {
    return "https://itunes.apple.com/cn/app/id1095089970?mt=8";
  } else {
    return "http://app.mi.com/details?id=com.wali.live";
  }
}

export function isIOS() {
  var _sysReg = [
    /(Android).*/i,
    /(iPhone|iPad|iPod|iOS).*/i,
    /(windows(.*|\s*)phone).*/i,
  ];
  var _ua = navigator.userAgent.toLowerCase();

  return _sysReg[1].test(_ua);
}

const IOS_LATEST_VERSION = 505086;
const checkIOSOld = () => {
  if (util.sys.isIOS()) {
    let ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf("-") != -1) {
      let versions = ua.split("mizhibo-")[1].split("-")[1];

      let frontCode = versions.split(".")[0],
        midCode = versions.split(".")[1],
        afterCode = versions.split(".")[2];

      let versionNum =
        parseInt(frontCode * 100000) +
        parseInt(midCode * 1000) +
        parseInt(afterCode);

      if (versionNum < parseInt(IOS_LATEST_VERSION)) {
        return 1;
      } else {
        return 0;
      }
    }
  } else {
    return 0;
  }
};

export const CRITICAL_IOS_VERSION = "5.5.101";
export const CRITICAL_ANDROID_VERSION = "5.14.89";
/**
 * 检查版本
 */
export function checkAppVersionIsNew() {
  let ua = navigator.userAgent.toLowerCase();
  try {
    if (ua.indexOf("-") != -1) {
      let versions = ua
        .split("mizhibo-")[1]
        .split("-")[1]
        .split(".");

      let frontCode = versions[0],
        midCode = versions[1],
        afterCode = versions[2];

      let frontVersionCode, midVersionCode, afterVersionCode;

      if (util.sys.isAndroid()) {
        let androidVersions = CRITICAL_ANDROID_VERSION.split(".");
        frontVersionCode = androidVersions[0];
        midVersionCode = androidVersions[1];
        afterVersionCode = androidVersions[2];
      } else {
        let iosVersions = CRITICAL_IOS_VERSION.split(".");
        frontVersionCode = iosVersions[0];
        midVersionCode = iosVersions[1];
        afterVersionCode = iosVersions[2];
      }

      if (
        frontCode < frontVersionCode ||
        (frontCode === frontVersionCode &&
          (midCode < midVersionCode ||
            (midCode === midVersionCode && afterCode <= afterVersionCode)))
      ) {
        return false;
      } else if (
        frontCode > frontVersionCode ||
        (frontCode === frontVersionCode &&
          (midCode > midVersionCode ||
            (midCode === midVersionCode && afterCode > afterVersionCode)))
      ) {
        return true;
      }
    } else {
      return false;
    }
  } catch (e) {
    return false;
  }
}

// 默认导出的变量
export default {
  uuid,
  zuid,
  st,
  tab,
  showPk,
  checkIOSOld,
};

/**
 * 获取当前时间
 * @param {参数} data
 */
export const getTime = async () => {

  return await axios.get(GET_CUR_TIME).then(function ({data}) {
    return data
  })
};

export const _pdDownTime = async () => {
  return await axios.get(GET_CUR_TIME).then(function ({data}) {
    if (data && data.code == 0) {
      let now = data.data.t;
      let nowDate=new Date(now)
      let end=nowDate.getMinutesEndDate(10)

      let timeSpace = end.getTime() - now;
      let leftSec = Math.floor(timeSpace / 1000);
      let str=getCountDownMinute(leftSec)
      return { str, nowDate };
    }

  })
};

export const leftPad = (num) => {
  return num < 10 ? `0${num}` : num;
};

/**
 * 获取排行榜列表
 * @param {请求参数} data
 */
export const getRankList = async (data, type) => {
  // if (env == "test") return testRankList;

  let params, url;
  params = Object.assign({}, data, {
    uuid,
    st,
    actId: ACT_ID,
  });

  url = `${GET_INFO}/${ACT_ID}/rankInfo`;

  return await axios
    .get(url, {
      params,
    })
    .then(function({ data }) {
      return data;
    });
};

/**
 * 获取PK列表信息
 * @param data
 * @param type
 * @returns {Promise<AxiosResponse<any>>}
 */
export const getPKList = async () => {
  let params = Object.assign(
    {},
    {
      uuid,
      actId: ACT_ID,
      zuid: 0,
      pkGroupName: "pk_battle",
      st,
    }
  );

  let url = `${GET_PK}/${ACT_ID}/getPKList`;

  return await axios
    .get(url, {
      params,
    })
    .then(function({ data }) {
      return data;
    });
};

/**
 * 获取挂件PK列表信息
 * @param data
 * @param type
 * @returns {Promise<AxiosResponse<any>>}
 */
export const getWidgetPKList = async () => {
  let params = Object.assign(
    {},
    {
      uuid,
      actId: ACT_ID,
      zuid: 0,
      pkGroupName: "pk_battle",
      st,
    }
  );

  let url = `${GET_PK}/${ACT_ID}/getWidgetPKList`;

  return await axios
    .get(url, {
      params,
    })
    .then(function({ data }) {
      return data;
    });
};

/**
 * 告白墙
 */
export const getGiftRecord = async (_) => {
  let params = {
    actId: ACT_ID,
    limit: 10,
    zuid: 0,
    uuid,
    st,
  };

  let url = `${GET_INFO}/${ACT_ID}/getGiftRecord`;

  return await axios
    .get(url, {
      params,
    })
    .then(function({ data }) {
      return data;
    });
};

/**
 * task
 */
export const getTask = async (_) => {
  // uuid=10638
  let params = {
    uuid,
    actId: ACT_ID,
    period: 1,
    day: "",
    st,
  };
  let url = `${GET_SIGN}/${ACT_ID}/index`;

  return await axios
    .get(url, {
      params,
    })
    .then(function({ data }) {
      return data;
    });
};

/**
 * 获取主播信息
 */
export const getAnchorInfo = async (data) => {
  // zuid=10638
  let params = Object.assign({ rankType: 1 }, data, {
    zuid,
    uuid,
    needGap: true,
    needContribute: false,
    needRegister: false,
    needFollow: false,
    rankName,
    st,
  });
  let url = `${GET_INFO}/${ACT_ID}/anchorInfo`;

  return await axios
    .get(url, {
      params,
    })
    .then(function({ data }) {
      return data;
    });
};
/**
 * 获取用户信息
 */
export const getUserInfo = async (data) => {
  // zuid=10638
  let params = Object.assign({}, data, {
    zuid: uuid,
    uuid,
    rankType: 5,
    needGap: true,
    needContribute: false,
    needRegister: false,
    needFollow: false,
    rankName,
    st,
  });
  let url = `${GET_INFO}/${ACT_ID}/anchorInfo`;

  return await axios
    .get(url, {
      params,
    })
    .then(function({ data }) {
      return data;
    });
};

/**
 * 获取PK信息(挂件)
 */
export const getPKInfo = async (data) => {
  // zuid=12072
  let params = Object.assign({}, data, {
    uuid,
    actId: ACT_ID,
    zuid,
    pkGroupName: "pk_battle",
    st,
  });

  let url = `${GET_PK}/${ACT_ID}/getWidgetPKInfo`;

  return await axios
    .get(url, {
      params,
    })
    .then(function({ data }) {
      return data;
    });
};
//登录
export const getSign = async (data) => {
  let params = Object.assign({}, data, {
    uuid,
    period: 1,
    taskId: 1,
    st,
  });

  let url = `${GET_SIGN}/${ACT_ID}/sign`;

  return await axios
    .get(url, {
      params,
    })
    .then(function({ data }) {
      return data;
    });
};

/**
 * 抽奖次数
 */
export const getLotteryCnt = async () => {
  let params = {
    uuid,
    st,
    actId: ACT_ID,
  };
  let url = `${LOTTERY}/${ACT_ID}/getAcceptLotteryCnt`;
  return await axios
    .get(url, {
      params,
    })
    .then(function({ data }) {
      return data;
    });
};

/**
 * 抽奖
 */

export const lottery = async () => {
  let params = {
    uuid,
    actId: ACT_ID,
    st,
    ctype,
  };
  let url = `${LOTTERY}/${ACT_ID}/buyLottery`;
  return await axios
    .get(url, {
      params,
    })
    .then(function({ data }) {
      return data;
    });
};

// 获取当前赛段
export const getStageInfo = async (_) => {
  let params = {
    rankName: rankName,
    actId: ACT_ID,
    st,
  };
  let url = `${GET_INFO}/${ACT_ID}/getRankStage`;

  return await axios
    .get(url, {
      params,
    })
    .then(function({ data }) {
      return data;
    });
};
